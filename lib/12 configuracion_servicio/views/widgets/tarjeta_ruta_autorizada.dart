import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TarjetaAutorizada extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaAutorizada({Key key, this.size, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Colors.white,
      child: ListTile(
        leading: Icon(FontAwesome.bus, color: Color(hexColor('#3A4A64'))),
        title: Row(
          children: <Widget>[
             Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
               Text(
              'Ruta 1 - PCB2035',
              style: TextStyle(
                  fontSize: size, color: color, fontWeight: FontWeight.bold),
            ),
            Text('Fecha de autorización: 09/05/2020',
                style: TextStyle(
                  fontSize: 10,
                  color: Color(hexColor('#3F7EA3')),
                )),
            Text('Coordinador Asignado: Daniel Granda',
                style: TextStyle(
                  fontSize: 10,
                  color: Color(hexColor('#3F7EA3')),
                )),
          ],
        ),
         
     
          ],
        ),
       trailing: Icon(Icons.check_circle, color: Color(hexColor('#5CC4B8'))),
      ),
    );
  }
}
