import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TarjetaMonitor extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaMonitor({Key key, this.size, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Color(hexColor('#F6C34F')).withOpacity(0.8),
      child: ListTile(
        leading: Icon(FontAwesome.user_circle, color: Color(hexColor('#3A4A64'))),
        title: Row(
          children: <Widget>[
            Text(
              'Nombre: Daniel Granda',
              style: TextStyle(
                  fontSize: size, color: color, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(
                  Icons.mail,
                  color: Color(hexColor('#5CC4B8')),
                ),
                onPressed: () {},
              ),
            ),
            SizedBox( width: 20),
             IconButton(
                icon: Icon(
                  FontAwesome.phone,
                  color: Color(hexColor('#5CC4B8')),
                ),
                onPressed: () {},
              ),
          ],
        ),
        subtitle: Text(
          'Cédula: 1717084592',
          style: TextStyle(fontSize: 10,color: Color(hexColor('#3A4A64'))),
        ),
      ),
    );
  }
}
