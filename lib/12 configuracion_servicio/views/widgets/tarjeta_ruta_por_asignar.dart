import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:vibration/vibration.dart';

class TarjetaRutaPorAsignar extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaRutaPorAsignar({Key key, this.size, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> _negacionSalidaKey =
        GlobalKey<FormBuilderState>();
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Color(hexColor('#F6C34F')).withOpacity(0.7),
      child: ListTile(
        leading: Icon(FontAwesome.bus, color: Color(hexColor('#3A4A64'))),
        title: Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Ruta 1 - PCB2035',
                  style: TextStyle(
                      fontSize: size,
                      color: color,
                      fontWeight: FontWeight.bold),
                ),
                Text('Fecha de envio: 09/05/2020',
                    style: TextStyle(
                      fontSize: 10,
                      color: Color(hexColor('#3F7EA3')),
                    )),
                Text('Monitor: Daniel Granda',
                    style: TextStyle(
                      fontSize: 10,
                      color: Color(hexColor('#3F7EA3')),
                    )),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color:  Color(hexColor('#5CC4B8')))),
                  color: Color(hexColor('#3A4A64')),
                  child: Text('Asignar',
                      style: TextStyle(fontSize: 12, color: Colors.white)),
                  onPressed: () {
                    AwesomeDialog(
                        context: context,
                        dialogType: DialogType.WARNING,
                        animType: AnimType.SCALE,
                        tittle: 'Asignación',
                        body: FormBuilder(
                            key: _negacionSalidaKey,
                            initialValue: {
                              'date': DateTime.now(),
                              'accept_terms': false,
                            },
                            autovalidate: true,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: <Widget>[
                                Text(
                                  'Seleccione el coordinador para asignar la ruta',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color(hexColor('#3A4A64'))),
                                ),
                                FormBuilderDropdown(
                                  attribute: "coordinador",
                                  decoration:
                                      InputDecoration(labelText: "Coordinador"),
                                  icon: Icon(FontAwesome.user_circle,
                                      color: Color(hexColor('#61B4E5'))),
                                  //hint: Text('Seleccione el combustible'),
                                  validators: [
                                    FormBuilderValidators.required(
                                        errorText: 'Requerido')
                                  ],
                                  items: [
                                    'Daniel Granda',
                                    'Marcelo Granda',
                                    'Francisco Ocampo',
                                  ]
                                      .map((coord) => DropdownMenuItem(
                                          value: coord,
                                          child: Text(
                                            "$coord",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color:
                                                    Color(hexColor('#61B4E5'))),
                                          )))
                                      .toList(),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(children: <Widget>[])
                              ]),
                            )),
                        desc:
                            'Está apunto de negar el ingreso a la Unidad Educativa, ¿Desea continuar?',
                        btnOkColor: Color(hexColor('#5CC4B8')),
                        btnOkText: 'Aceptar',
                        btnOkOnPress: () async {
                          Navigator.popAndPushNamed(context, 'configServicio');
                          if (await Vibration.hasVibrator()) {
                            Vibration.vibrate();
                          }
                        },
                        btnCancelText: 'Cancelar',
                        btnCancelColor: Color(hexColor('#E86A87')),
                        btnCancelOnPress: () async {
                          if (await Vibration.hasVibrator()) {
                            Vibration.vibrate();
                          }
                        }).show();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
