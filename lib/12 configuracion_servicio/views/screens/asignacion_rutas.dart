import 'package:administradoraservicio/12%20configuracion_servicio/views/widgets/tarjeta_ruta_asignada.dart';
import 'package:administradoraservicio/12%20configuracion_servicio/views/widgets/tarjeta_ruta_autorizada.dart';
import 'package:administradoraservicio/12%20configuracion_servicio/views/widgets/tarjeta_ruta_por_asignar.dart';
import 'package:administradoraservicio/12%20configuracion_servicio/views/widgets/tarjeta_ruta_rechazada.dart';
import 'package:administradoraservicio/4%20autorizaciones_ingreso_salida/views/widgets/tarjeta_aprobacion.dart';
import 'package:administradoraservicio/4%20autorizaciones_ingreso_salida/views/widgets/tarjeta_no_aprobacion.dart';
import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';

class AsignacionRutas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Card(
                  color: Color(hexColor('#3F7EA3')),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/icon/IcoLlegada.png'))),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: double.infinity,
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Asignación de Rutas a Coordinadores',
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(height: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TarjetaRutaPorAsignar(
                            color: Colors.white,
                            size: 14,
                          ),
                          TarjetaRutaPorAsignar(
                            color: Colors.white,
                            size: 14,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  indent: 4,
                  thickness: 2,
                  color: Colors.blueGrey,
                ),
                Expanded(
                  flex: 1,
                  child: ListView(
                    children: <Widget>[
                      Card(
                        color: Color(hexColor('#3F7EA3')),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Rutas Asignadas',
                        ),
                      ),
                      TarjetaAsignada(
                        size: 12,
                        color: Color(hexColor('#F6C34F')),
                      ),
                      Card(
                        color: Color(hexColor('#3F7EA3')),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Rutas autorizada',
                        ),
                      ),
                      TarjetaAutorizada(
                        size: 12,
                        color: Color(hexColor('#5CC4B8')),
                      ),
                      TarjetaAutorizada(
                        size: 12,
                        color: Color(hexColor('#5CC4B8')),
                      ),
                      Card(
                        color: Color(hexColor('#3F7EA3')),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Rutas rechazadas',
                        ),
                      ),
                      TarjetaRechazada(
                        size: 12,
                        color: Color(hexColor('#E86A87')),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
