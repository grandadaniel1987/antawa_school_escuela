import 'package:administradoraservicio/9%20Perfil_usuario/views/widgets/cabecera_perfil.dart';
import 'package:administradoraservicio/9%20Perfil_usuario/views/widgets/fondoRegistro.dart';
import 'package:administradoraservicio/9%20Perfil_usuario/views/widgets/formulario_perfil.dart';
import 'package:administradoraservicio/9%20Perfil_usuario/views/widgets/tarjeta_perfil.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';

class PerfilUserUi extends StatefulWidget {
  @override
  _PerfilUserUiState createState() => _PerfilUserUiState();
}

//UserBloc userBloc;

class _PerfilUserUiState extends State<PerfilUserUi> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          FondoRegistro(),
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      CabeceraPerfil(),
                      TarjetaPerfilFoto(
                        title: 'Colegio Paulo Sexto',
                        urlImg: 'assets/icon/cole.png',
                        tipoEPS: 'Unidad Educativa\nFundación: 5/10/1964',
                      ),
                      SizedBox(height: 10),
                      Card(
                                              child: Text(
                          'Slogan: Nuestro compromiso con la juventud ecuatoriana',
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 14),
                        ),
                      ),
                      Divider(
                        color: Color(hexColor('#3A4A64')),
                        thickness: 3,
                        height: 30,
                      ),
                      TarjetaPerfilFoto(
                        title: 'Marcelo Granda',
                        urlImg: 'assets/marce.png',
                        tipoEPS: 'Representante Legal',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FormularioPerfil(),
                      SizedBox(
                        height: 10,
                      ),
                      TarjetaPerfilFoto(
                        title: 'Fracisco Cevallos',
                        tipoEPS: 'Administrador',
                        urlImg: 'assets/daniel.png',
                      ),
                      FormularioPerfil(),
                      TarjetaPerfilFoto(
                        title: 'Oscar Rivas',
                        urlImg: 'assets/daniel.png',
                        tipoEPS: 'Coordinador',
                      ),
                      FormularioPerfil(),
                      Divider(),
                      SizedBox(
                        height: 200,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
