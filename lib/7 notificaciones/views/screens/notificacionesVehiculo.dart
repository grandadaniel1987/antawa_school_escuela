import 'package:administradoraservicio/3%20monitoreo_de_ruta/views/widgets/cabecera_unidad_educativa.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vibration/vibration.dart';

class Notificaciones extends StatefulWidget {
  Notificaciones({Key key}) : super(key: key);

  @override
  _NotificacionesState createState() => _NotificacionesState();
}

class _NotificacionesState extends State<Notificaciones> {
  @override
  Widget build(BuildContext context) {
    //var user = Provider.of<LoginState>(context, listen: false).currentUser();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexColor('#3A4A64')),
        leading: IconButton(
          icon: Icon(
            FontAwesomeIcons.chevronCircleLeft,
            color: Color(hexColor('#5CC4B8')),
            size: 30,
          ),
          onPressed: () async {
            Navigator.popAndPushNamed(context, '/');
            if (await Vibration.hasVibrator()) {
              Vibration.vibrate();
            }
          },
        ),
        title: CabeceraUnidadEducativa(),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.3), BlendMode.dstIn),
              image: AssetImage("assets/backGround/FondoBlancoATW.png"),
              fit: BoxFit.contain,
            )),
          ),
          Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  CircleAvatar(
                    radius: 20,
                    backgroundImage:
                        AssetImage('assets/icon/notificaciones.png'),
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Sus Notificaciones',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'Poppins-Bold',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Color(hexColor('#5CC4B8')),
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.filter_list,
                            color: Color(hexColor('#5CC4B8'))),
                        onPressed: () {
                            showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime.now()
                                          .subtract(Duration(hours: 24 * 360)),
                                      lastDate: DateTime(2025),
                                      //locale: Locale('es', 'ES'),
                                    );
                        },
                      ),
                    ],
                  ),
                  Divider(),
                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Card(
                          elevation: 8,
                          //  color: Colors.white70,
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Color(hexColor('#5CC4B8')),
                              child: Icon(
                                Icons.apps,
                                color: Colors.white,
                              ),
                              radius: 20,
                            ),
                            title: Text(
                              'Nueva actulización',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Poppins-Medium',
                                fontSize: 12,
                                color: Colors.black,
                              ),
                            ),
                            subtitle: Text(
                              'Descargue la nueva actualización',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Poppins-Medium',
                                fontSize: 10,
                                color: Colors.blueGrey,
                              ),
                            ),
                            trailing: Text(
                              '05-ene',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Poppins-Medium',
                                fontSize: 10,
                                color: Colors.blueGrey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
