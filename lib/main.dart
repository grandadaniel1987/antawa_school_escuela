import 'package:administradoraservicio/prueba.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';


import '1 user_signIn_sigmUp/views/screens/ingresoAtw.dart';
import '1 user_signIn_sigmUp/views/screens/registro_atw.dart';
import '10 registro_usuarios/views/screens/1 detalle_rutas_aprobadas.dart';
import '10 registro_usuarios/views/screens/aprob_rutas_menu.dart';
import '10 registro_usuarios/views/widgets/aprob_vehiculo.dart';
import '10 registro_usuarios/views/widgets/mapa_estudiantes.dart';
import '11 estadisticas/views/screens/estadisticas_menu.dart';
import '12 configuracion_servicio/views/screens/configuración_monitor_menu.dart';
import '12 configuracion_servicio/views/screens/registro_monitor.dart';
import '12 configuracion_servicio/views/widgets/add_monitor.dart';
import '2 home_escuela/homePage/home_escuela.dart';
import '3 monitoreo_de_ruta/views/screens/monitoreo_ruta_menu.dart';
import '3 monitoreo_de_ruta/views/screens/permitionGpsUI.dart';
import '3 monitoreo_de_ruta/views/widgets/map_ruta_especifica.dart';
import '9 Perfil_usuario/views/screens/perfil_user.dart';
import '3 monitoreo_de_ruta/views/screens/info_abordo_no_abordo_noved.dart';
import '3 monitoreo_de_ruta/views/widgets/info_abordo_no_abordo.dart';
import '3 monitoreo_de_ruta/views/widgets/info_ruta_detalle.dart';
import '4 autorizaciones_ingreso_salida/views/screens/autorizaciones_menu.dart';
import '5 gestion_novedades/views/screens/gestion_novedades_menu.dart';
import '6 gestion_quejas/views/screens/gestion_quejas_menu.dart';
import '6 gestion_quejas/views/widgets/mensaje_quejas_bus.dart';
import '6 gestion_quejas/views/widgets/mensaje_quejas_tutor.dart';
import '6 gestion_quejas/views/widgets/tarjeta_quejas_tutor.dart';
import '7 notificaciones/views/screens/notificacionesVehiculo.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Empresa Administradora del Servicio',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(hexColor('#3A4A64')),
         fontFamily: 'Poppins-Medium',
      ),
      initialRoute:'ingreso' ,
      routes: {
        //HOME
        '/': (_) => HomeEscuela1(),
        'prueba': (_) => Prueba(),
        'perfil': (_) => PerfilUserUi(),
        //INGRESO
        'ingreso': (_) => IngresoAtwEscuela(),
        'registro': (_) => RegistroAtwEscuela(),
        //MONITOREO DE RUTA
        'permisosGps': (_) => PermitionGpsUi(),
        'flota': (_) => MapRutaEspecifica(),
        'monitoreo': (_) => MonitoreoRutaUnidadEducativa(),
        'infoRutas': (_) => InfoRutasDetalle(),
        'infoAbordo': (_) => InfoAbordoNoAbordo(),
        //AUTORIZACIONES
        'autorizacionesMenu': (_) => AutorizacionesMenu(),
        //NOVEDADES
        'novedades': (_) => GestionNovedades(),
        'infoAbordoNovedadesEst': (_) => InfoAbordoNoAbordoNovedades(),
        //QUEJAS
        'quejas': (_) => GestionQuejas(),
        'quejastutor': (_) => TarjetaQuejaTutor(),
        'quejasMenTutor': (_) => MensajeQuejaTutor(),
        'quejasMenBus': (_) => MensajeQuejaBus(),
        //NOTIFICACIONES
        'notificaciones': (_) => Notificaciones(),
        //APROBACIONES RUTAS
        'detalleAprobRutas': (_) => DetalleRutasAprobadas(),
        'aprobRutas': (_) => MonitorServicesMenu(),
        'aprobVehiculo': (_) => AprobVehiculoEps(),
        'aprobMapa': (_) => MapaEstudiantes(),
        //ESTADISTICAS
        'estadisticas': (_) => Estadisticas(),
        // CONFIGURACION MONITOR
        'configServicio': (_) => ConfiguracionServicio(),
        'addMonitor': (_) => AddMonitor(),
        'regMonitor': (_) => ResgistroMonitor(),



      },
    );
  }
}
