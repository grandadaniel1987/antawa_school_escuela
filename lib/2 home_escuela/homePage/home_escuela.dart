import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

class HomeEscuela1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double alto = MediaQuery.of(context).size.height;
    double ancho = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: cabecera(),
      drawer: menuLateral(context),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          fondo(),
          Column(
            children: <Widget>[
              botonMonitoreo(alto, ancho, context),
              botonAutorizaciones(alto, ancho, context),
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    botonNovedades(alto, ancho, context),
                    botonQuejas(alto, ancho, context),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    botonNotificaciones(alto, ancho, context),
                    botonEstadisticas(alto, ancho, context),
                    botonSalir(alto, ancho, context),
                  ],
                ),
              )
            ],
          )
          /* Container(
            child: Expanded(
              child: ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      ListTile(
                        title: Text('Henry Urrea'),
                        subtitle: Text('Coordinador Empresa Administradora'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ), */
        ],
      ),
    );
  }

  AppBar cabecera() {
    return AppBar(
        centerTitle: true,
        //backgroundColor: Color(hexColor('#F6C34F')),
        title: ListTile(
          trailing: CircleAvatar(
            backgroundImage: AssetImage('assets/icon/icoEps.png'),
          ),
          title: Text(
            'Granda Jaramillo Luis M...',
            style: TextStyle(
              fontFamily: 'Poppins-Medium',
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          subtitle: Text(
            'Coordinador Unidad Educativa',
            style: TextStyle(
                fontFamily: 'Poppins-Medium',
                fontSize: 12,
                //fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ));
  }

  Flexible botonMonitoreo(double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'monitoreo');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#3A4A64')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 10,
                          width: ancho / 2,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/icon/monitoreo.png'))),
                        ),
                        Text(
                          'Monitoreo de Rutas',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 16,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonNovedades(double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'novedades');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#61B4E5')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 14,
                          width: ancho / 3,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/icon/novedades.png'))),
                        ),
                        Text(
                          'Gestión de novedades',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 14,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonNotificaciones(
      double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'notificaciones');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#F6C34F')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 14,
                          width: ancho / 4,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/icon/notific.png'))),
                        ),
                        Text(
                          'Notificaciones',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 12,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonEstadisticas(double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'estadisticas');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#F6C34F')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 14,
                          width: ancho / 4,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/icon/estadisticas.png'))),
                        ),
                        Text(
                          'Estadísticas',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 12,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonSalir(double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          AwesomeDialog(
              context: context,
              dialogType: DialogType.WARNING,
              animType: AnimType.TOPSLIDE,
              tittle: 'Confirmación de Salida',
              desc: '¿Desea salir del sistema?',
              btnOkColor: Color(hexColor('#5CC4B8')),
              btnOkText: 'Aceptar',
              btnOkOnPress: () async {
                Navigator.popAndPushNamed(context, 'ingreso');
                if (await Vibration.hasVibrator()) {
                  Vibration.vibrate();
                }
              },
              btnCancelText: 'Cancelar',
              btnCancelColor: Color(hexColor('#E86A87')),
              btnCancelOnPress: () async {
                if (await Vibration.hasVibrator()) {
                  Vibration.vibrate();
                }
              }).show();
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#F6C34F')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 14,
                          width: ancho / 4,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/icon/salir.png'))),
                        ),
                        Text(
                          'Salir',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 12,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonQuejas(double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'quejas');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#61B4E5')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 14,
                          width: ancho / 3,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/icon/quejas.png'))),
                        ),
                        Text(
                          'Gestión de quejas',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 14,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Flexible botonAutorizaciones(
      double alto, double ancho, BuildContext context) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'autorizacionesMenu');
        },
        child: Card(
          elevation: 15,
          color: Colors.transparent,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Color(hexColor('#3A4A64')).withOpacity(0.9),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: alto / 10,
                          width: ancho / 2,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/icon/autorizaciones.png'))),
                        ),
                        Text(
                          'Autorizaciones',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: 16,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Container fondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.only(top: 10.0),
      decoration: BoxDecoration(
          image: DecorationImage(
        colorFilter: ColorFilter.mode(
            //Colors.black.withOpacity(0.2), BlendMode.colorBurn),
            Colors.black.withOpacity(0.1),
            BlendMode.colorBurn),
        // image: AssetImage("assets/backGround/backGroundAtw.png"),
        image: AssetImage("assets/backGround/FondoBlancoATW.png"),
        fit: BoxFit.contain,
      )),
    );
  }

  SafeArea menuLateral(BuildContext context) {
    return SafeArea(
      child: Container(
        width: 270,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            CircleAvatar(
              radius: 40,
              backgroundImage: AssetImage('assets/icon/icoEps.png'),
            ),
            SizedBox(height: 15),
            Divider(
              height: 15,
              color: Color(hexColor('#3A4A64')),
              thickness: 5,
            ),
            SizedBox(height: 5),
            Container(
              color: Colors.white30,
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/icon/perfil.png'),
                  radius: 25,
                ),
                title: Text(
                  'Perfil',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Poppins-Medium',
                    fontSize: 12,
                    color: Color(hexColor('#3A4A64')),
                  ),
                ),
                trailing: Icon(
                  (Icons.arrow_forward_ios),
                ),
                onTap: () async {
                  print('object');
                  Navigator.pushNamed(context, 'perfil');
                  if (await Vibration.hasVibrator()) {
                    Vibration.vibrate();
                  }
                },
              ),
            ),
            Divider(),
            Container(
              color: Colors.white30,
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/icon/config.png'),
                  radius: 25,
                ),
                title: Text(
                  'Configuración del servicio_',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Poppins-Medium',
                    fontSize: 12,
                    color: Color(hexColor('#3A4A64')),
                  ),
                ),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () async {
                  Navigator.pushNamed(context, 'configServicio');
                  if (await Vibration.hasVibrator()) {
                    Vibration.vibrate();
                  }
                },
              ),
            ),
            Divider(),
            Container(
              color: Colors.white30,
              child: ListTile(
                onTap: () async {
                  Navigator.pushNamed(context, 'detalleAprobRutas');
                  if (await Vibration.hasVibrator()) {
                    Vibration.vibrate();
                  }
                },
                leading: Container(
                  width: 40,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 10),
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage: AssetImage('assets/icon/auth.png'),
                        radius: 13,
                      ),
                    ],
                  ),
                ),
                title: Text(
                  'Aprobación de Rutas',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Poppins-Medium',
                    fontSize: 12,
                    color: Color(hexColor('#3A4A64')),
                  ),
                ),
                trailing: Icon(
                  (Icons.arrow_forward_ios),
                ),
              ),
            ),
            Divider(),
            Container(
              color: Colors.white30,
              child: ListTile(
                onTap: () {
                  AwesomeDialog(
                      context: context,
                      dialogType: DialogType.WARNING,
                      animType: AnimType.TOPSLIDE,
                      tittle: 'Confirmación de Salida',
                      desc: '¿Desea salir del sistema?',
                      btnOkColor: Color(hexColor('#5CC4B8')),
                      btnOkText: 'Aceptar',
                      btnOkOnPress: () async {
                        Navigator.popAndPushNamed(context, 'ingreso');
                        if (await Vibration.hasVibrator()) {
                          Vibration.vibrate();
                        }
                      },
                      btnCancelText: 'Cancelar',
                      btnCancelColor: Color(hexColor('#E86A87')),
                      btnCancelOnPress: () async {
                        if (await Vibration.hasVibrator()) {
                          Vibration.vibrate();
                        }
                      }).show();
                },
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/icon/exit.png'),
                  radius: 25,
                ),
                title: Text(
                  'Cerrar Sesión',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Poppins-Medium',
                    fontSize: 12,
                    color: Color(hexColor('#3A4A64')),
                  ),
                ),
                trailing: Icon(
                  (Icons.arrow_forward_ios),
                ),
              ),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
