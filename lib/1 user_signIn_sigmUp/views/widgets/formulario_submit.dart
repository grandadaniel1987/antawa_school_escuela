import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormularioAndSubmit extends StatelessWidget {
  const FormularioAndSubmit({
    Key key,
    @required GlobalKey<FormBuilderState> ingresoAdminKey,
    @required this.colorField,
  })  : _ingresoAdminKey = ingresoAdminKey,
        super(key: key);

  final GlobalKey<FormBuilderState> _ingresoAdminKey;
  final Color colorField;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: _ingresoAdminKey,
      initialValue: {
        'date': DateTime.now(),
        'accept_terms': false,
      },
      autovalidate: true,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            FormBuilderTextField(
              keyboardType: TextInputType.emailAddress,
              attribute: "email",
              decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.lightGreenAccent, width: 0.0),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.redAccent, width: 0.0),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.orange, width: 0.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.lightGreenAccent, width: 0.0),
                  ),
                  suffixIcon: Icon(Icons.alternate_email, color: colorField),
                  /* border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)), */
                  labelText: "Email",
                  labelStyle: TextStyle(fontSize: 14, color: colorField),
                  helperStyle: TextStyle(fontSize: 12, color: colorField),
                  hintText: 'Ingrese su email',
                  hintStyle: TextStyle(color: Colors.white54, fontSize: 12)),
              style: TextStyle(fontSize: 14, color: colorField),
              validators: [
                FormBuilderValidators.required(
                  errorText: 'Requerido',
                ),
                FormBuilderValidators.email(
                    errorText: 'Debe ser un correo válido')
              ],
            ),
            SizedBox(
              height: 10,
            ),
            FormBuilderTextField(
              keyboardType: TextInputType.emailAddress,
              attribute: "Password",
              decoration: InputDecoration(
                  suffixIcon: Icon(Icons.lock, color: colorField),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.lightGreenAccent, width: 0.0),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.redAccent, width: 0.0),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.orange, width: 0.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.lightGreenAccent, width: 0.0),
                  ),
                  labelText: "Contraseña",
                  labelStyle: TextStyle(fontSize: 14, color: colorField),
                  helperStyle: TextStyle(fontSize: 12, color: colorField),
                  hintText: 'Ingrese su contraseña',
                  hintStyle: TextStyle(color: Colors.white54, fontSize: 12)),
              style: TextStyle(fontSize: 14, color: colorField),
              validators: [
                FormBuilderValidators.required(
                  errorText: 'Requerido',
                ),
                FormBuilderValidators.minLength(6,
                    errorText: 'Mínimo 6 caracteres')
              ],
            ),
            SizedBox(
              height: 25,
            ),
//Submit

            InkWell(
              borderRadius: BorderRadius.circular(20),
              child: Container(
                width: 170,
                height: 50,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color(hexColor('#3F7EA3')),
                      Color(hexColor('#61B4E5')),
                      Color(hexColor('#3F7EA3'))
                    ]),
                    borderRadius: BorderRadius.circular(6.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.white.withOpacity(0.2),
                          offset: Offset(0.0, 8.0),
                          blurRadius: 8.0)
                    ]),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {
                      if (_ingresoAdminKey.currentState.saveAndValidate()) {
                        print(
                          _ingresoAdminKey.currentState.value,
                        );
                        Navigator.pushReplacementNamed(context, '/');

                        /*    final _auth = FirebaseAuth.instance;

    if (_ingresoPadresKey.currentState.saveAndValidate()) {
      print(
        _ingresoPadresKey.currentState.value,
      );
      await _auth
          .createUserWithEmailAndPassword(
        email: _ingresoPadresKey.currentState.value['email'],
        password: _ingresoPadresKey.currentState.value['Password'],
      )
          .then((user) {
        print('registro exitoso con el usuario:${user.user.email} ');
        //Provider.of<LoginState>(context, listen: false).login();
        Navigator.pushNamed(context, '/');
      }).catchError((e) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(e.message),
        ));
      }); */

                        /*      if (user!=null) {
Provider.of<LoginState>(context, listen: false).login();
        
      } */
                      } else {
                        AwesomeDialog(
                                btnCancelColor: Color(hexColor('#E86A87')),
                                context: context,
                                dialogType: DialogType.ERROR,
                                animType: AnimType.TOPSLIDE,
                                tittle: 'Verificación',
                                desc: 'Verifique los datos para el ingreso',
                                btnCancelText: 'Aceptar',
                                btnCancelOnPress: () {})
                            .show();
                      }
                    },
                    child: Center(
                      child: Text("INGRESAR",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Poppins-Bold",
                              fontSize: 18,
                              letterSpacing: 1.0)),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
