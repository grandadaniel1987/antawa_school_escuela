
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';

class PienIngreso extends StatelessWidget {
    final String titlePrincipal;
    final String titleSecundary;
    final String rute;
  const PienIngreso({
    Key key, @required this.titlePrincipal, @required this.titleSecundary, @required this.rute,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, rute);
      },
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            titlePrincipal,
            style: TextStyle(
                fontFamily: "Poppins-Medium", color: Colors.grey),
          ),
          InkWell(
            onTap: () {},
            child: Text(titleSecundary,
                style: TextStyle(
                    color: Color(hexColor('#F6C34F')),
                    fontFamily: "Poppins-Bold")),
          )
        ],
      ),
    );
  }
}
