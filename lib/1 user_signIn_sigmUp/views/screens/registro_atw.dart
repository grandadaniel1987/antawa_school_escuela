import 'package:administradoraservicio/1%20user_signIn_sigmUp/views/widgets/cabecera_ingreso.dart';
import 'package:administradoraservicio/1%20user_signIn_sigmUp/views/widgets/fondo_ingreso.dart';
import 'package:administradoraservicio/1%20user_signIn_sigmUp/views/widgets/formulario_submit.dart';
import 'package:administradoraservicio/1%20user_signIn_sigmUp/views/widgets/pie_ingreso.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class RegistroAtwEscuela extends StatefulWidget {
  RegistroAtwEscuela({Key key}) : super(key: key);

  @override
  _RegistroAtwEscuelaState createState() => _RegistroAtwEscuelaState();
}

class _RegistroAtwEscuelaState extends State<RegistroAtwEscuela> {
  final GlobalKey<FormBuilderState> _ingresoAdminKey =
      GlobalKey<FormBuilderState>();

  Color colorField = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          FondoIngreso(),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    CabeceraIngreso(
                      title: 'REGISTRO',
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    FormularioAndSubmit(
                        ingresoAdminKey: _ingresoAdminKey,
                        colorField: colorField),
                    SizedBox(
                      height: 30,
                    ),
                    PienIngreso(
                            rute: 'ingreso',
                      titlePrincipal: 'ya tienes cuenta? ',
                      titleSecundary: 'Ingresa',
                    ),
                    SizedBox( height: 20),
                    //SocialSignIn(),
                    SizedBox(
                      height: 300,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
