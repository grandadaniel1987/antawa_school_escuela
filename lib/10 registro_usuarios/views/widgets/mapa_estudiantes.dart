import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:flutter/material.dart';

class MapaEstudiantes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: <Widget>[
        ImagenFondoApp(),
        Column(
          children: <Widget>[
            Container(
              height: queryData.size.height / 2,
              width: queryData.size.height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/icon/ruta.jpg'),
                      fit: BoxFit.cover)),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Card(
                    elevation: 4,
                    child: ListTile(
                      leading: CircleAvatar(child: Text('1')),
                      title: Text('Estudiante: Daniel Granda'),
                      subtitle: Text('Representante: Marcelo Granda'),
                      trailing: Icon(Icons.map),
                    ),
                  ),
                  Divider(),
                        Card(
                    elevation: 4,
                    child: ListTile(
                      leading: CircleAvatar(child: Text('2')),
                      title: Text('Estudiante: Marco Zapata'),
                      subtitle: Text('Representante: Gerardo Zapata'),
                      trailing: Icon(Icons.map),
                    ),
                  )
                ],
              ),
            )
          ],
        )
      ]),
    );
  }
}
