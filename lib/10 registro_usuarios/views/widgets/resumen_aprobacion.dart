import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class ResumenAprobacion extends StatelessWidget {
  const ResumenAprobacion({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> _resumenAprobKey =
        GlobalKey<FormBuilderState>();
  /*   TextStyle estiloInf = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: Color(hexColor('#5CC4B8'))); */

    //var user = Provider.of<LoginState>(context, listen: false).currentUser();
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Expanded(
                    child: SingleChildScrollView(
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      elevation: 5,
                      color: Colors.white,
                      child: Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 5),
                                FormBuilder(
                                    key: _resumenAprobKey,
                                    initialValue: {
                                      'date': DateTime.now(),
                                      'accept_terms': false,
                                    },
                                    autovalidate: true,
                                    child: Column(children: <Widget>[
                                      Icon(Icons.supervised_user_circle, size: 35,),
                                      TitulosEstado(
                                          title:
                                              'Observaciones Monitor - Conductor',
                                          color: Color(hexColor('#F6C34F'))),
                                      SizedBox(height: 10),
                                      FormBuilderTextField(
                                        readOnly: true,
                                        textCapitalization:
                                            TextCapitalization.characters,
                                        attribute: "comentarios",
                                        decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            labelText: 'Observaciones',
                                            labelStyle: TextStyle(
                                                fontSize: 14,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            helperStyle: TextStyle(
                                                fontSize: 12,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            hintText:
                                                'Sus observaciones previo aprobación'),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#3A4A64'))),
                                      ),
                                      SizedBox(height: 10),
                                      Icon(FontAwesome.bus, size: 30,),
                                      TitulosEstado(
                                          title: 'Observaciones Vehículo',
                                          color: Color(hexColor('#E86A87'))),
                                      SizedBox(height: 10),
                                      FormBuilderTextField(
                                        readOnly: true,
                                        textCapitalization:
                                            TextCapitalization.characters,
                                        attribute: "comentarios",
                                        decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            labelText: 'Observaciones',
                                            labelStyle: TextStyle(
                                                fontSize: 14,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            helperStyle: TextStyle(
                                                fontSize: 12,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            hintText:
                                                'Sus observaciones previo aprobación'),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#3A4A64'))),
                                        validators: [
                                          FormBuilderValidators.required(
                                            errorText: 'Requerido',
                                          ),
                                          FormBuilderValidators.minLength(8,
                                              errorText: 'Error de ingreso'),
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      Icon(FontAwesome.map_marker, size: 30,),
                                      TitulosEstado(
                                          title: 'Observaciones Ruta',
                                          color: Color(hexColor('#61B4E5'))),
                                      SizedBox(height: 10),
                                      FormBuilderTextField(
                                        readOnly: true,
                                        textCapitalization:
                                            TextCapitalization.characters,
                                        attribute: "comentarios",
                                        decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            labelText: 'Observaciones',
                                            labelStyle: TextStyle(
                                                fontSize: 14,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            helperStyle: TextStyle(
                                                fontSize: 12,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            hintText:
                                                'Sus observaciones previo aprobación'),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#3A4A64'))),
                                      ),
                                    ])),
                                SizedBox(height: 10),
                                Row(
                                  children: <Widget>[
                                    Expanded(child: SizedBox()),
                                    FlatButton(
                                      onPressed: () {
                                

                                          AwesomeDialog(
                                              btnOkColor:
                                                  Color(hexColor('#5CC4B8')),
                                              context: context,
                                              dialogType: DialogType.SUCCES,
                                              animType: AnimType.TOPSLIDE,
                                              tittle: 'Validación Exitosa',
                                              desc: 'Se ha validado una ruta',
                                              //btnCancelOnPress: () {},
                                              btnOkOnPress: () {
                                                Navigator.pushNamed(context,
                                                    'detalleAprobRutas');
                                              }).show();
                                   
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: AssetImage(
                                                'assets/icon/ok.png'),
                                          ),
                                          Text("Validar"),
                                        ],
                                      ),
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, 'detalleAprobRutas');
                                        //_fase1Key.currentState.reset();
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: AssetImage(
                                                'assets/icon/cancel.png'),
                                          ),
                                          Text("Cancelar"),
                                        ],
                                      ),
                                    ),
                                    Expanded(child: SizedBox()),
                                  ],
                                )
                              ],
                            ),
                          ))),
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
