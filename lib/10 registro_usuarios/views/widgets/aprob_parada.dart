import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class AprobParada extends StatelessWidget {
  const AprobParada({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> _aprobVehiculoKey =
        GlobalKey<FormBuilderState>();
    TextStyle estiloInf = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: Color(hexColor('#5CC4B8')));

    //var user = Provider.of<LoginState>(context, listen: false).currentUser();
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Container(
                          child: ExpandablePanel(
                        header: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(FontAwesome.map_marker,
                                    color: Color(hexColor('#3A4A64'))),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'Datos de la Ruta',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Color(hexColor('#3A4A64'))),
                                ),
                              ],
                            ),
                            Divider()
                          ],
                        ),
                        collapsed: Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, 'aprobMapa');
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Ruta',
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Color(hexColor('#5CC4B8')))),
                                  CircleAvatar(
                                    radius: 35,
                                    backgroundImage:
                                        AssetImage('assets/icon/ruta.jpg'),
                                  )
                                ],
                              ),
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Nombre de la Ruta', style: estiloInf),
                                Text('La Magadalena')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Tipo de Ruta', style: estiloInf),
                                Text('Ida')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Jornanda', style: estiloInf),
                                Text('Matutina')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Hora de inicio', style: estiloInf),
                                Text('06:58')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Hora de finalización de ruta',
                                    style: estiloInf),
                                Text('08:00')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Sector', style: estiloInf),
                                Text('El valle')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Número de paradas', style: estiloInf),
                                Text('5')
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Número de estudiantes', style: estiloInf),
                                Text('8')
                              ],
                            ),
                          ],
                        )),
                      )),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                    child: SingleChildScrollView(
                  child: Card(
                             shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      color: Colors.white,
                      child: Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 5),
                                /*     Text(
                                      'Revisión',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold, fontSize: 16),
                                    ),
 */
                                FormBuilder(
                                    key: _aprobVehiculoKey,
                                    initialValue: {
                                      'date': DateTime.now(),
                                      'accept_terms': false,
                                    },
                                    autovalidate: true,
                                    child: Column(children: <Widget>[
                                      FormBuilderTextField(
                                        textCapitalization:
                                            TextCapitalization.characters,
                                        attribute: "comentarios",
                                        decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            labelText: 'Observaciones',
                                            labelStyle: TextStyle(
                                                fontSize: 14,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            helperStyle: TextStyle(
                                                fontSize: 12,
                                                color:
                                                    Color(hexColor('#3A4A64'))),
                                            hintText:
                                                'Sus observaciones previo aprobación'),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#3A4A64'))),
                                        validators: [
                                          FormBuilderValidators.required(
                                            errorText: 'Requerido',
                                          ),
                                          FormBuilderValidators.minLength(8,
                                              errorText: 'Error de ingreso'),
                                        ],
                                      ),
                                    ])),
                                SizedBox(height: 90),
                                Row(
                                  children: <Widget>[
                                    Expanded(child: SizedBox()),
                                    FlatButton(
                                      onPressed: () {
                                        if (_aprobVehiculoKey.currentState
                                            .saveAndValidate()) {
                                          print(
                                            _aprobVehiculoKey
                                                .currentState.value,
                                          );

                                          AwesomeDialog(
                                              btnOkColor:
                                                  Color(hexColor('#5CC4B8')),
                                              context: context,
                                              dialogType: DialogType.SUCCES,
                                              animType: AnimType.TOPSLIDE,
                                              tittle: 'Registro Exitoso',
                                              desc:
                                                  'Se ha guardado las observaciones',
                                              //btnCancelOnPress: () {},
                                              btnOkOnPress: () {
                                                Navigator.pushNamed(context,
                                                    'detalleAprobRutas');
                                              }).show();
                                        } else {
                                          print('campos por validar');
                                          AwesomeDialog(
                                                  btnOkColor: Color(
                                                      hexColor('#E86A87')),
                                                  context: context,
                                                  dialogType: DialogType.ERROR,
                                                  animType: AnimType.TOPSLIDE,
                                                  tittle: 'Error de Registro ',
                                                  desc:
                                                      'Revise campos obligatorios',
                                                  //btnCancelOnPress: () {},
                                                  btnOkOnPress: () {
                                                    /* Navigator.pushNamed(context, 'registroMonitor'); */
                                                  })
                                              .show();
                                        }
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: AssetImage(
                                                'assets/icon/ok.png'),
                                          ),
                                          Text("Guardar"),
                                        ],
                                      ),
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, 'detalleAprobRutas');
                                        //_fase1Key.currentState.reset();
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: AssetImage(
                                                'assets/icon/cancel.png'),
                                          ),
                                          Text("Cancelar"),
                                        ],
                                      ),
                                    ),
                                    Expanded(child: SizedBox()),
                                  ],
                                )
                              ],
                            ),
                          ))),
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
