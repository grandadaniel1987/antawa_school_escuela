import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';

class CardRutasEstadoPrincipal extends StatelessWidget {
  const CardRutasEstadoPrincipal({
    Key key,
    @required this.iconEstado, @required this.estado, this.onTapEstado,
  }) : super(key: key);

  final Icon iconEstado;
  final String estado;
  final VoidCallback onTapEstado;

  @override
  Widget build(BuildContext context) {
      TextStyle estiloCard = TextStyle(fontSize: 11);
    return Card(
      elevation: 5,
      child: ListTile(
        onTap: onTapEstado,
        title: Row(
          children: <Widget>[
            iconEstado,
            SizedBox(width: 10),
            Text('Ruta: Solanda'),
            Expanded(
                child: SizedBox(
              width: 0,
            )),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.edit,
                    color: Color(hexColor('##F6C34F')))),
          ],
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(estado,
                    style: estiloCard),
              ],
            ),
            Text(
              '- Vehículo: PCX-0011',
              style: estiloCard,
            ),
            Text('- Tpo: Ida', style: estiloCard),
            Text('- Conductor: Daniel Granda', style: estiloCard),
            Text('- Monitor: Santiago Carvajal', style: estiloCard),
            Text('- Estudiantes: 15', style: estiloCard),
          ],
        ),
      ),
    );
  }
}
