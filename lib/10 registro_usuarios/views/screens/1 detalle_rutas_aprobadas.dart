import 'package:administradoraservicio/10%20registro_usuarios/views/widgets/estado_card_style.dart';
import 'package:administradoraservicio/9%20Perfil_usuario/views/widgets/fondoRegistro.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:vibration/vibration.dart';

class DetalleRutasAprobadas extends StatefulWidget {
  DetalleRutasAprobadas({Key key}) : super(key: key);

  @override
  _DetalleRutasAprobadasState createState() => _DetalleRutasAprobadasState();
}

class _DetalleRutasAprobadasState extends State<DetalleRutasAprobadas> {
  @override
  Widget build(BuildContext context) {
    //var user = Provider.of<LoginState>(context, listen: false).currentUser();
    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(
            FontAwesome.chevron_circle_left,
            color: Color(hexColor('#61B4E5')),
            size: 30,
          ),
          onPressed: () async {
            Navigator.popAndPushNamed(context, '/');
            if (await Vibration.hasVibrator()) {
    Vibration.vibrate();
}
          },
        ),
        backgroundColor: Colors.white,
        title: Text('Rutas Aprobadas'.toUpperCase(),
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Color(hexColor('#5CC4B8')),
            )),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          FondoRegistro(),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListView(
              children: <Widget>[
                TitulosEstado(
                  title: 'Rutas en proceso de aprobación',
                  color: Color(hexColor('#F6C34F')),
                ),
                SizedBox(height: 10),
                CardRutasEstadoPrincipal(
                  estado:
                      '- Estado: En proceso de aprobación, \ndesde el 28-02-2020',
                  iconEstado:
                      Icon(Icons.warning, color: Color(hexColor('#F6C34F'))),
                  onTapEstado: () async {
                    if (await Vibration.hasVibrator()) {
                      Vibration.vibrate();
                    }
                    Navigator.popAndPushNamed(context, 'aprobRutas');
                  },
                ),
                SizedBox(height: 10),
                TitulosEstado(
                  title: 'Rutas aprobadas',
                  color: Color(hexColor('#5CC4B8')),
                ),
                CardRutasEstadoPrincipal(
                  estado: '- Estado: Aprobada: el 28-02-2020',
                  iconEstado:
                      Icon(Icons.check_box, color: Color(hexColor('#5CC4B8'))),
                  onTapEstado: () {},
                ),
                SizedBox(height: 10),
                TitulosEstado(
                  title: 'Rutas rechazadas',
                  color: Color(hexColor('#E86A87')),
                ),
                CardRutasEstadoPrincipal(
                  estado: '- Estado: Desaprobada: el 28-02-2020',
                  iconEstado:
                      Icon(Icons.cancel, color: Color(hexColor('#E86A87'))),
                  onTapEstado: () {},
                ),
                SizedBox(height: 10),
              ],
            ),
          )
        ],
      ),
    );
  }
}
