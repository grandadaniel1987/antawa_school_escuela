import 'package:administradoraservicio/10%20registro_usuarios/views/widgets/aprob_monitor.dart';
import 'package:administradoraservicio/10%20registro_usuarios/views/widgets/aprob_parada.dart';
import 'package:administradoraservicio/10%20registro_usuarios/views/widgets/aprob_vehiculo.dart';
import 'package:administradoraservicio/10%20registro_usuarios/views/widgets/resumen_aprobacion.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';



class MonitorServicesMenu extends StatefulWidget {
  MonitorServicesMenu({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new HomeWidgetState();
  }
}

class HomeWidgetState extends State<MonitorServicesMenu>
    with SingleTickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    new Tab(text: "Aprobación de Conductor y Monitor"),
    new Tab(text: "Aprobación de Vehículo"),
    new Tab(text: "Aprobación de Ruta"),
    new Tab(text: "Resumen"),
    //new Tab(text: "Vinculación a Vehículos"),
    //new Tab(text: "Vinculación a Empresa")
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(
            FontAwesome.chevron_circle_left,
            color: Color(hexColor('#61B4E5')),
            size: 30,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'detalleAprobRutas');
          },
        ),
        backgroundColor: Colors.white,
        title: Text('Registro de Usuarios'.toUpperCase(),
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Color(hexColor('#5CC4B8')),
            )),
        bottom: new TabBar(
          isScrollable: true,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.white,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: new BubbleTabIndicator(
            indicatorHeight: 25.0,
            indicatorColor: Color(hexColor('#F6C34F')),
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
          ),
          tabs: tabs,
          controller: _tabController,
        ),
      ),
      body: new TabBarView(controller: _tabController, children: <Widget>[
        AprobConductorMonitor(),
        AprobVehiculoEps(),
        AprobParada(),
        ResumenAprobacion(),
        //AddVehiculos(),
        //Text(''),
        //Text(''),
      ]),
    );
  }
}

