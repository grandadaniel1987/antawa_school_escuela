import 'package:administradoraservicio/3%20monitoreo_de_ruta/views/screens/info_estudiantes_rutas_noved.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TarjetaRetrasoTemporal extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaRetrasoTemporal({Key key, this.size, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          
          Container(
            child: ExpandablePanel(
              hasIcon: false,
              header: Column(
                children: <Widget>[

                  Card(
                      shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
  ),
                    elevation: 10,
                    color: Color(hexColor('#61B4E5')),
                    child: ListTile(
                      leading: Icon(
                        FontAwesome.bus,
                        color: Colors.white,
                      ),
                      trailing: Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 30,),
                      title: Row(
                        children: <Widget>[
                          Text(
                            'Ruta 1 - PCB2035',
                            style: TextStyle(fontSize: size, color: color),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Conductor: Daniel Granda',
                            style: TextStyle(fontSize: 10),
                          ),
                          Text(
                            'Hora de notificación: 10:00',
                            style: TextStyle(fontSize: 10),
                          ),
                          Text(
                            'Tiempo Aprox: 00:5 min',
                            style: TextStyle(fontSize: 10),
                          ),
                            Text(
                            'Motivo: Tráfico',
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              collapsed: Container(child: InfoEstudiantesRutaNovedades()),
            ),
          ),
        ],
      ),
    );
  }
}
