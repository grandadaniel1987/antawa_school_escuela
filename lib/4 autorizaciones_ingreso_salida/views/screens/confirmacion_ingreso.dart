import 'package:administradoraservicio/4%20autorizaciones_ingreso_salida/views/widgets/tarjeta_aprob_ingresos.dart';
import 'package:administradoraservicio/4%20autorizaciones_ingreso_salida/views/widgets/tarjeta_aprobacion.dart';
import 'package:administradoraservicio/4%20autorizaciones_ingreso_salida/views/widgets/tarjeta_no_aprobacion.dart';
import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';

class ConfirmacionIngreso extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Card(
                  color: Color(hexColor('#3A4A64')),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/icon/UnidadEduca.png'))),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: double.infinity,
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Ingresos en Proceso de Autorización',
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(height: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TarjetaAprobacionIngresos(
                            color: Colors.white,
                            size: 14,
                          ),
                          TarjetaAprobacionIngresos(
                            color: Colors.white,
                            size: 14,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  indent: 4,
                  thickness: 2,
                  color: Colors.blueGrey,
                ),
                Expanded(
                  flex: 1,
                  child: ListView(
                    children: <Widget>[
                      Card(
                        color: Color(hexColor('#3A4A64')),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: TitulosEstado(
                          color: Color(hexColor('#5CC4B8')),
                          title: 'Ingresos Autorizados',
                        ),
                      ),
                      TarjetaAprobacionResumen(
                        size: 12,
                        color: Color(hexColor('#5CC4B8')),
                      ),
                      TarjetaAprobacionResumen(
                        size: 12,
                        color: Color(hexColor('#5CC4B8')),
                      ),
                      Card(
                        color: Color(hexColor('#3A4A64')),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: TitulosEstado(
                          color: Color(hexColor('#E86A87')),
                          title: 'Ingresos no autorizados',
                        ),
                      ),
                      TarjetaNoAprob(
                        size: 12,
                        color: Color(hexColor('#E86A87')),
                      ),
                      TarjetaNoAprob(
                        size: 12,
                        color: Color(hexColor('#E86A87')),
                      ),
                      TarjetaNoAprob(
                        size: 15,
                        color: Color(hexColor('#E86A87')),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
