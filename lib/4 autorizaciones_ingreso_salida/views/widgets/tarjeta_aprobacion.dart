import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TarjetaAprobacionResumen extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaAprobacionResumen({Key key, this.size, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
  ),
      color: Colors.white.withOpacity(0.9),
      child: ListTile(
        leading: Icon(
          FontAwesome.bus,
        ),
        title: Row(
          children: <Widget>[
            Text(
              'Ruta 1 - PCB2035',
              style: TextStyle(fontSize: size, color: color),
            ),
         
          ],
        ),
        subtitle: Text(
          'Conductor: Daniel Granda',
          style: TextStyle(fontSize: 10),
        ),
        trailing: Icon(
                  FontAwesome.check_circle,
                  color: Color(hexColor('#5CC4B8')),
                ),
      ),
    );
  }
}
