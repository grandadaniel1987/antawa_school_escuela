import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:vibration/vibration.dart';

class TarjetaAprobacionSalida extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaAprobacionSalida({Key key, this.size, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> _negacionSalidaKey =
        GlobalKey<FormBuilderState>();
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Color(hexColor('#F6C34F')).withOpacity(0.7),
      child: ListTile(
        leading: Icon(FontAwesome.bus, color: Color(hexColor('#3A4A64'))),
        title: Row(
          children: <Widget>[
            Text(
              'Ruta 1 - PCB2035',
              style: TextStyle(
                  fontSize: size, color: color, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(
                  FontAwesome.check_circle,
                  color: Color(hexColor('#5CC4B8')),
                ),
                onPressed: () {
                  AwesomeDialog(
                      context: context,
                      dialogType: DialogType.SUCCES,
                      animType: AnimType.TOPSLIDE,
                      tittle: 'Confirmación de Salida',
                      desc:
                          'Está apunto de autorizar el salida del vehículo de la Unidad Educativa, ¿Desea continuar?',
                      btnOkColor: Color(hexColor('#5CC4B8')),
                      btnOkText: 'Aceptar',
                      btnOkOnPress: () async {
                        Navigator.popAndPushNamed(context, '/');
                        if (await Vibration.hasVibrator()) {
                          Vibration.vibrate();
                        }
                      },
                      btnCancelText: 'Cancelar',
                      btnCancelColor: Color(hexColor('#E86A87')),
                      btnCancelOnPress: () async {
                        if (await Vibration.hasVibrator()) {
                          Vibration.vibrate();
                        }
                      }).show();
                },
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.cancel,
                color: Color(hexColor('##E86A87')),
              ),
              onPressed: () {
                AwesomeDialog(
                    context: context,
                    dialogType: DialogType.ERROR,
                    animType: AnimType.SCALE,
                    tittle: 'Negación de Salida',
                    body: FormBuilder(
                        key: _negacionSalidaKey,
                        initialValue: {
                          'date': DateTime.now(),
                          'accept_terms': false,
                        },
                        autovalidate: true,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(children: <Widget>[
                            Text('Seleccione el motivo de no autorización de ingreso', textAlign: TextAlign.center, style: TextStyle(color:  Color(hexColor('#3A4A64'))),),
                            
                            FormBuilderDropdown(
                              attribute: "motivo",
                              decoration: InputDecoration(labelText: "Motivo"),
                              icon: Icon(FontAwesome.bus,
                                  color: Color(hexColor('#61B4E5'))),
                              //hint: Text('Seleccione el combustible'),
                              validators: [
                                FormBuilderValidators.required(
                                    errorText: 'Requerido')
                              ],
                              items: [
                                'No concuerda el número de estudiantes',
                                'Vehículo en mal estado',
                                'Imperdimento conductor o  monitor',
                              ]
                                  .map((tipo) => DropdownMenuItem(
                                      value: tipo,
                                      child: Text(
                                        "$tipo",
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#61B4E5'))),
                                      )))
                                  .toList(),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[])
                          ]),
                        )),
                    desc:
                        'Está apunto de negar el ingreso a la Unidad Educativa, ¿Desea continuar?',
                    btnOkColor: Color(hexColor('#5CC4B8')),
                    btnOkText: 'Aceptar',
                    btnOkOnPress: () async {
                      Navigator.popAndPushNamed(context, '/');
                      if (await Vibration.hasVibrator()) {
                        Vibration.vibrate();
                      }
                    },
                    btnCancelText: 'Cancelar',
                    btnCancelColor: Color(hexColor('#E86A87')),
                    btnCancelOnPress: () async {
                      if (await Vibration.hasVibrator()) {
                        Vibration.vibrate();
                      }
                    }).show();
              },
            ),
          ],
        ),
        subtitle: Text(
          'Conductor: Daniel Granda',
          style: TextStyle(fontSize: 10),
        ),
      ),
    );
  }
}
