import 'package:administradoraservicio/3%20monitoreo_de_ruta/views/widgets/cabecera_unidad_educativa.dart';
import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vibration/vibration.dart';

class MensajeQuejaTutor extends StatelessWidget {
  final double size;
  final Color color;

  const MensajeQuejaTutor({Key key, this.size, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormBuilderState> _quejaTutorKey =
        GlobalKey<FormBuilderState>();
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(hexColor('#3A4A64')),
          leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.chevronCircleLeft,
              color: Color(hexColor('#5CC4B8')),
              size: 30,
            ),
            onPressed: () {
              Navigator.popAndPushNamed(context, 'quejas');
            },
          ),
          title: CabeceraUnidadEducativa(),
        ),
        body: Stack(fit: StackFit.expand, children: <Widget>[
          ImagenFondoApp(),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/icon/quejas.png'))),
                ),
                Text(
                  'Gestión de quejas',
                  style: TextStyle(
                    fontFamily: 'Poppins-Bold',
                    fontSize: 16,
                    color: Color(hexColor('#61B4E5')),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Card(
                    elevation: 8,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            FormBuilder(
                              key: _quejaTutorKey,
                              initialValue: {
                                'date': DateTime.now(),
                                'accept_terms': false,
                              },
                              autovalidate: true,
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 20),
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: Container(
                                      color: Color(hexColor('#5CC4B8'))
                                          .withOpacity(0.3),
                                      child: Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                  'Del representante: Daniel Granda',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              'Hora de notificación: 10:00',
                                              style: TextStyle(fontSize: 10),
                                            ),
                                            Text(
                                              'Categoria: Servicio',
                                              style: TextStyle(fontSize: 10),
                                            ),
                                            Divider(
                                              color: Colors.white,
                                              height: 30,
                                            ),
                                            Text(
                                              'Detalle: El conductor no llega a tiempo',
                                              style: TextStyle(fontSize: 10),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Divider(),
                                  Text(
                                    'Responder',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Color(hexColor('#E86A87')),
                                    ),
                                  ),
                                  FormBuilderTextField(
                                    keyboardType:TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    attribute: "quejaReps",
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        labelText: "Respuesta al usuario",
                                        labelStyle: TextStyle(
                                            fontSize: 14,
                                            color: Color(hexColor('#3A4A64'))),
                                        helperStyle: TextStyle(
                                            fontSize: 12,
                                            color: Color(hexColor('#61B4E5'))),
                                        hintText: 'Detalle su respuesta'),
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Color(hexColor('#61B4E5'))),
                                    validators: [
                                      FormBuilderValidators.required(
                                        errorText: 'Requerido',
                                      ),
                                      FormBuilderValidators.minLength(10,
                                          errorText: 'Mínimo 10 caracteres')
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  FlatButton(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Container(
                                        width: 150,
                                        color: Color(hexColor('#5CC4B8')),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Enviar Respuesta',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      if (_quejaTutorKey.currentState
                                          .saveAndValidate()) {
                                        print(
                                          _quejaTutorKey.currentState.value,
                                        );

                                        AwesomeDialog(
                                            context: context,
                                            dialogType: DialogType.WARNING,
                                            animType: AnimType.TOPSLIDE,
                                            tittle: 'Verificación',
                                            desc:
                                                '¿Desea enviar respuesta a la queja? ',
                                            btnOkText: 'Aceptar',
                                            btnOkColor:
                                                Color(hexColor('#5CC4B8')),
                                            btnOkOnPress: () async {
                                              if (await Vibration
                                                  .hasVibrator()) {
                                                Navigator.popAndPushNamed(
                                                    context, '/');
                                                Flushbar(
                                                  backgroundColor: Color(
                                                      hexColor('#5CC4B8')),
                                                  flushbarPosition:
                                                      FlushbarPosition.TOP,
                                                  message:
                                                      "Su queja ha sido enviada con éxito",
                                                  icon: Icon(
                                                    Icons.check_circle,
                                                    size: 28.0,
                                                    color: Color(
                                                        hexColor('#2E3B52')),
                                                  ),
                                                  duration:
                                                      Duration(seconds: 5),
                                                  leftBarIndicatorColor:
                                                      Colors.blue[300],
                                                )..show(context);

                                                Vibration.vibrate();
                                              }
                                            },
                                            btnCancelText: 'Cancelar',
                                            btnCancelColor:
                                                Color(hexColor('##E86A87')),
                                            btnCancelOnPress: () {
                                              Navigator.popAndPushNamed(
                                                  context, 'quejas');
                                            }).show();
                                      }
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ]));
  }
}
