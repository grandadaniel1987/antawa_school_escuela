import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TarjetaQuejaBus extends StatelessWidget {
  final double size;
  final Color color;

  const TarjetaQuejaBus({Key key, this.size, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            elevation: 8,
            // color: Color(hexColor('#5CC4B8')),
            child: ListTile(
              leading: Icon(
                FontAwesome.envelope,
                color: Color(hexColor('#5CC4B8')),
              ),
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Color(hexColor('#5CC4B8')),
                size: 30,
              ),
              title: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Para el representante:',
                        style: TextStyle(fontSize: size, color: color),
                      ),
                      Text(
                        'Julio Armas',
                        style: TextStyle(fontSize: size, color: color),
                      ),
                    ],
                  ),
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'De la ruta : 1 PXS-0021',
                    style: TextStyle(fontSize: 10),
                  ),
                  Text(
                    'Hora de notificación: 10:00',
                    style: TextStyle(fontSize: 10),
                  ),
                  Text(
                    'Categoria: Servicio',
                    style: TextStyle(fontSize: 10),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, 'quejasMenBus');
              },
            ),
          ),
        ],
      ),
    );
  }
}
