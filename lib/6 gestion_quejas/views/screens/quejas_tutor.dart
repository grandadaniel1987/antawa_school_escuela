import 'package:administradoraservicio/6%20gestion_quejas/views/widgets/tarjeta_quejas_tutor.dart';
import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';

class QuejasTutor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      SizedBox(height: 10),
                      SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                                  Card(
                              color: Color(hexColor('#3A4A64')),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              elevation: 5,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(height: 5),
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/icon/representante.png'))),
                                  ),
                                  SizedBox(width: 10),
                                  Container(
                                    width: double.infinity,
                                    child: TitulosEstado(
                                      color: Color(hexColor('#F6C34F')),
                                      title: 'Quejas del Representante Legal',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            TarjetaQuejaTutor(
                              size: 12,
                            ),
                              TarjetaQuejaTutor(
                              size: 12,
                            ),
                              TarjetaQuejaTutor(
                              size: 12,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
