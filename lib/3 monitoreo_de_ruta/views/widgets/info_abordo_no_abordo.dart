import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'cabecera_unidad_educativa.dart';
import 'inf_tutor_llarmar.dart';

class InfoAbordoNoAbordo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexColor('#3A4A64')),
        leading: IconButton(
          icon: Icon(
            FontAwesomeIcons.chevronCircleLeft,
            color: Color(hexColor('#5CC4B8')),
            size: 30,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'infoRutas');
          },
        ),
        title: CabeceraUnidadEducativa(),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
               Card(

                 
                  color: Color(hexColor('#3A4A64')),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 8,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/icon/representante.png'))),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: double.infinity,
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Información de Estudiantes Abordo',
                        ),
                      ),
                    ],
                  ),
                ),    
                SizedBox(height: 10),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TitulosEstado(
                            color: Color(hexColor('#5CC4B8')),
                            title: 'Estudiantes que abordaron',
                          ),
                          InfoEstuTutorLlamar(),
                          InfoEstuTutorLlamar(),
                          InfoEstuTutorLlamar(),
                          TitulosEstado(
                            color: Color(hexColor('#F6C34F')),
                            title: 'Estudiantes por abordar',
                          ),
                          InfoEstuTutorLlamar(),
                          InfoEstuTutorLlamar(),
                          TitulosEstado(
                            color: Color(hexColor('#E86A87')),
                            title: 'Estudiantes que no abordaron',
                          ),
                          InfoEstuTutorLlamar(),
                          InfoEstuTutorLlamar(),
                          InfoEstuTutorLlamar(),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Divider(),
        ],
      ),
    );
  }
}
