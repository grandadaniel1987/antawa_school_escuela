import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class InfoMonitorConductorRuta extends StatelessWidget {
  const InfoMonitorConductorRuta({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white.withOpacity(0.9),
      child: ListTile(
        //leading: Icon(FontAwesome.bus),
        title: Row(
          children: <Widget>[
            Icon(FontAwesome.bus),
            SizedBox(width: 10),
            Text('Ruta 1 - PCB2035'),
            SizedBox(width: 10),
          ],
        ),
        subtitle: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(width: 35),
                Text('Conductor: Daniel Granda'),
                Expanded(child: SizedBox(width: 0)),
                IconButton(
                  icon: Icon(FontAwesome.phone,
                      color: Color(hexColor('#5CC4B8'))),
                  onPressed: () {},
                ),
              ],
            ),
            Row(
              children: <Widget>[
                SizedBox(width: 35),
                Text('Monitor: Marcelo Granda'),
                Expanded(child: SizedBox(width: 0)),
                IconButton(
                  icon: Icon(FontAwesome.phone,
                      color: Color(hexColor('#5CC4B8'))),
                  onPressed: () {},
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
