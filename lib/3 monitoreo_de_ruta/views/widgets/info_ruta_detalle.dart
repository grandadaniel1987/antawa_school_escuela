import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'cabecera_unidad_educativa.dart';
import 'inf_conductor_monitor.dart';
import 'info_estudiantes_rutas.dart';
import 'map_ruta_especifica.dart';


class InfoRutasDetalle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexColor('#3A4A64')),
        leading: IconButton(
          icon: Icon(
            FontAwesomeIcons.chevronCircleLeft,
            color: Color(hexColor('#5CC4B8')),
            size: 30,
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'monitoreo');
          },
        ),
        title: CabeceraUnidadEducativa(),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: <Widget>[
                    SizedBox(height: 5),
                    InfoMonitorConductorRuta(),
                    InfoEstudiantesRuta(),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  child: MapRutaEspecifica(),
                ),
              )
            ],
          ),
          //Divider(),
        ],
      ),
    );
  }
}


