
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class InfoRutasGeneral extends StatelessWidget {
  const InfoRutasGeneral({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
  ),
      color:  Color(hexColor('#F6C34F')).withOpacity(0.8),
                    child: ListTile(
        leading: Icon(FontAwesome.bus),
        title: Text('Ruta 1 - PCB2035', style: TextStyle(fontSize: 14),),
        subtitle: Text('Conductor: Daniel Granda', style: TextStyle(fontSize: 12),),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: (){
          Navigator.pushNamed(context, 'infoRutas');
        },
      ),
    );
  }
}
