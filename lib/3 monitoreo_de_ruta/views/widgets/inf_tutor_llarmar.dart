
import 'package:flutter/material.dart';

class InfoEstuTutorLlamar extends StatelessWidget {
  const InfoEstuTutorLlamar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
        shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
  ),
          child: ListTile(
        leading: Icon(Icons.supervised_user_circle),
        title: Text('Estudiante Luis Armas', style: TextStyle(fontSize: 14),),
        subtitle: Text('Tutor: Marcelo Arias', style: TextStyle(fontSize: 12),),
        trailing: IconButton(icon: Icon(Icons.phone),
        onPressed: (){},
        ),
      ),
    );
  }
}