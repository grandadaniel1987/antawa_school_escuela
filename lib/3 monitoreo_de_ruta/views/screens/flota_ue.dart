import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:vibration/vibration.dart';

class Flota extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          //ImagenFondoApp(),

          //MapRutaEspecifica(),
          Text('Flota: 14 Unidades'),
          SizedBox(height: 10),

          Center(
              child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Container(
              color: Color(hexColor('#5CC4B8')),
              width: 140,
              height: 40,
              child: FlatButton.icon(
                  onPressed: () async {
                    if (await Vibration.hasVibrator()) {
                      Vibration.vibrate();
                    }
                    Navigator.pushNamed(context, 'flota');
                  },
                  icon: Icon(
                    FontAwesome.map_marker,
                    color: Colors.white,
                  ),
                  label: Text(
                    'Mi Flota',
                    style: TextStyle(color: Colors.white),
                  )),
            ),
          )),
        ],
      ),
    );
  }
}
