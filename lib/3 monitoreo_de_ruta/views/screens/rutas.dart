import 'package:administradoraservicio/3%20monitoreo_de_ruta/views/widgets/inf_rutas_general.dart';
import 'package:administradoraservicio/utils/fondo_monitoreo.dart';
import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:administradoraservicio/utils/titulos_estado.dart';
import 'package:flutter/material.dart';



class Rutas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
  
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImagenFondoApp(),
          Column(
            children: <Widget>[
                   Card(
                  color: Color(hexColor('#3A4A64')),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/icon/UnidadEduca.png'))),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: double.infinity,
                        child: TitulosEstado(
                          color: Color(hexColor('#F6C34F')),
                          title: 'Rutas Aprobadas',
                        ),
                      ),
                    ],
                  ),
                ),
              Expanded(
                              child: ListView(
                  children: <Widget>[
                 
                    SizedBox( height: 5),
                    InfoRutasGeneral(),
                    Divider(),
                    InfoRutasGeneral(),
                    Divider(),
                    InfoRutasGeneral(),
                    Divider(),
                    InfoRutasGeneral(),
                    Divider(),
                    InfoRutasGeneral(),
                    Divider(),
                      InfoRutasGeneral(),
                    Divider(),
                      InfoRutasGeneral(),
                    Divider(),
                      InfoRutasGeneral(),
                    Divider(),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
