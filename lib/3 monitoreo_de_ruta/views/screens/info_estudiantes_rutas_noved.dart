import 'package:administradoraservicio/utils/hexaColor.dart';
import 'package:flutter/material.dart';

class InfoEstudiantesRutaNovedades extends StatelessWidget {
  const InfoEstudiantesRutaNovedades({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Text('Estudiantes en Ruta'),
          Expanded(
            child: FlatButton(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  color: Color(hexColor('#5CC4B8')),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Más información',
                      style: TextStyle(
                          fontSize: 10, color: Colors.white),
                    ),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'infoAbordoNovedadesEst');
              },
            ),
          ),
        ],
      ),
      subtitle: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Estudiantes abordo:'),
              Text('5'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Estudiantes que no abordaron:'),
              Text('2'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Estudiantes por abordar:'),
              Text('3'),
            ],
          ),
        ],
      ),
    );
  }
}

